package org.spring.springboot.controller;

import org.spring.springboot.dubbo.CityDubboConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CityController {
	
	@Autowired
	private CityDubboConsumerService cityDubboConsumerService;
	
	@GetMapping("/city")
	public void queryCity(){
		cityDubboConsumerService.printCity();
	}
}
