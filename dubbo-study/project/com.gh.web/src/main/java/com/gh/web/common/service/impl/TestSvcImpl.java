package com.gh.web.common.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.gh.center.test.bean.User;
import com.gh.center.test.service.ITestService;
import com.gh.web.common.service.TestSvc;

public class TestSvcImpl implements TestSvc {

	@Autowired
	private ITestService TestService;
	
	public String getResult(User user) {
		return TestService.print(user);
//		return "hello";
	}

}
