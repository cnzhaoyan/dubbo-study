package com.gh.web.common;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.gh.center.test.bean.User;
import com.gh.web.common.service.TestSvc;

@Controller
@RequestMapping(value="/hello")
public class TestController {
	
	@Autowired
	private TestSvc testSvc;
	
	@RequestMapping(value="/login")
	public ModelAndView method1(HttpServletRequest req,Map<String, Object> model){
		//uid = req.getParameter("id");
		System.out.println("---------method1---------");
		
		req.setAttribute("msg", "hello world");
		model.put("key", "value");
		return new ModelAndView("hello");
	}
	
	@RequestMapping(value="/view")
	@ResponseBody
	public String method2(HttpServletRequest req){
		String name = req.getParameter("name");
		int age = Integer.parseInt(req.getParameter("age"));
		String userId = req.getParameter("userId");
		User user = new User(userId, age, name);
//		System.out.println(testSvc.getResult(user));
		return testSvc.getResult(user);
//		return null;
	}

}
