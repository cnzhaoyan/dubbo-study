<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <% 
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+ ((request.getServerPort()==80)?"":":"+request.getServerPort()) +path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script src="<%=basePath %>js/jquery-1.8.3.min.js"></script>
<title>Insert title here</title>
<script type="text/javascript">
   var basePath = "<%=basePath %>";
   function clickButton(){
	   var userId = $('#userId').val();
	   var name = $('#name').val();
	   var age = $('#age').val();
	   $.ajax({
			url:basePath+"hello/view",
			contentType:"application/x-www-form-urlencoded; charset=utf-8", 
			type:"GET",
			dataType:"json",
			cache:false,
			data: {name:name,age:age,userId:userId},
			success:function(response){
				if(response===1){
					alert("保存成功！");
				}else{
					alert("保存失败！");
				}
			},
			error:function(){
				alert("保存失败！可能是断网或者其他原因！");
			}
		});	
   }
</script>
</head>
<body>
<div>this is hello jsp!</div>
姓名：<input type="text" id="name"/><br/>
年龄：<input type="text" id="age"/><br/>
用户id:<input type="text" id="userId"/><br/>
<button id="jamp" onclick="clickButton()">保存</button>
</body>
</html>