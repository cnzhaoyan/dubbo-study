package com.gh.center.test.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.gh.center.test.bean.User;
import com.gh.center.test.dao.TestDao;

public class TestDaoImpl implements TestDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	public int insert(User user) {
		String sql = "INSERT INTO user(id,name,age) VALUES(?,?,?) ";
		int res = jdbcTemplate.update(sql, new Object[]{user.getId(),user.getName(),user.getAge()});
		return res;
	}

}
