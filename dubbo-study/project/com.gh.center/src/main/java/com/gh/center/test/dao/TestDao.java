package com.gh.center.test.dao;

import com.gh.center.test.bean.User;

public interface TestDao {
	
	public int insert(User user);

}
