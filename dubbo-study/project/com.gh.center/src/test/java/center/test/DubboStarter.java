package center.test;

import com.ailk.dazzle.util.AppContext;
import com.alibaba.dubbo.container.Main;

public class DubboStarter {

	private static void startDubbo(String[] args)
	{
		AppContext.getApplicationContext();
		Main.main(args);
	}

	public static void main(String[] args)
	{
		startDubbo(args);
	}

}
